<?php
$PathPDF = filter_input(INPUT_GET, "PathPDF");
$PathPNG = "/usr/local/poligon/homes/o/osipovav/public_html/development/ascon.loodsmandoc/lib/modules/additionWM_1.0/loodsmandoc_wm.png";
$cmd = '/usr/local/bin/java1.8 -jar AdditionWM-PDF.jar "'.$PathPDF.'" "'.$PathPNG.'" "-"';
$shell_exec = shell_exec($cmd);
$pathinfo = pathinfo($PathPDF);
if (($pathinfo['extension'] === "pdf") || ($pathinfo['extension'] === "PDF")){
    $cmd = '/usr/local/bin/java1.8 -jar AdditionWM-PDF.jar "'.$PathPDF.'" "'.$PathPNG.'" "-"';
    $shell_exec = shell_exec($cmd);
    $pathinfo = pathinfo($PathPDF);
    header('Content-Disposition: attachment; filename="'.$pathinfo['basename'].'"');
    header("Content-type: application/pdf");
    echo $shell_exec;
}elseif (($pathinfo['extension'] === "vrp") || ($pathinfo['extension'] === "VRP")) {
    session_start();
    include_once('elah2.php');
    $_NameUser = $PHP_AUTH_USER;
    $_IdObject = $_SESSION['CurObj-id'];
    error_reporting(E_ALL);
    @$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
    if ($socket === false) {exit(0);}
    @$result = socket_connect($socket, '192.168.233.235', 11002);
    if ($result === false) exit(0);
    $in = "<root><NameFunc>WebLoodsmanDoc-GetTechDoc</NameFunc><NameDB>ST3D</NameDB><NameUser>$_NameUser</NameUser>
        <NameObject></NameObject><IdObject>$_IdObject</IdObject></root>";
    @$byte = serialize($in);
    socket_write($socket, $byte, strlen($byte));
    $out = socket_read($socket, 10240);
    $out1 = "";
    for ($n=16; $n< strlen($out); $n++){
        $out1 = $out1 . $out[$n];
    }
    for ($n=0; $n< strlen($out1); $n++) {
        if ($out1[$n] === '\\') {
            $out1[$n] = '/';
        }
    }
    socket_close($socket);
    $_path_pdf = $_SESSION['_full_path_obj'] . $out1;
    $cmd = '/usr/local/bin/java1.8 -jar AdditionWM-PDF.jar "'.$_path_pdf.'" "'.$PathPNG.'" "-"';
    $shell_exec = shell_exec($cmd);
    $pathinfo = pathinfo($PathPDF);
    header('Content-Disposition: attachment; filename="'.$pathinfo['basename'].'"');
    header("Content-type: application/pdf");
    echo $shell_exec;
}