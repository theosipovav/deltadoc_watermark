import com.itextpdf.text.*;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ByteArrayOutputStream;

public class main {

    public static void main(String[] args) throws IOException, DocumentException {
        /*
        args[0] - путь до .pdf файла
        args[1] - режим работы:
         - 1 - Водный знак формируется для Электронной КДиТПП
         - 2 - Водный знак формируется для Справочной КД
         */
        String sPathPdf = args[0];
        String sMode = args[1];
        if (sPathPdf.length() == 0){
            System.out.println("Неверно заданы входные параметры: отсутствует путь до .pdf файла (args[0])");
            return;
        }
        if (sMode.length() == 0){
            System.out.println("Неверно заданы входные параметры: отсутствует режим работы (args[1])");
            return;
        }
        String sResult = "";
        switch (sMode){
            case "1":
                /*
                Водный знак формируется для Электронной КДиТПП
                args[0] - путь до изменяемого .pdf файла
                args[2] - путь до картинки для вводного знака
                */
                String sPathImg = args[2];
                sResult = fAddWMtoPdfm1(sPathPdf, sPathImg);
                //sResult = fAddWMtoPdfm1_outFile(sPathPdf, sPathImg, "d:/OUT.pdf");
                break;
            case "2":
                /*
                Водный знак формируется для Справочной КД
                args[0] - путь до изменяемого .pdf файла
                args[2] - путь до первой картинки для вводного знака
                args[3] - путь до второй картинки для вводного знака
                args[4] - путь до текста (для второй картинки) для вводного знака
                */
                String sPathImg1 = args[2];
                String sPathImg2 = args[3];
                String sNumberCD = args[4];
                //sResult = fAddWMtoPdfm2(sPathPdf, sPathImg1, sPathImg2, sNumberCD);
                sResult = fAddWMtoPdfm2_outFile(sPathPdf, sPathImg1, sPathImg2, sNumberCD, "d:/OUT2.pdf");
                break;
            default:
                System.out.println("Неизвестный режим");

                break;
        }
        System.out.println(sResult);
    }

    private static String fAddWMtoPdfm1(String sPathPdf, String sPathImg) throws IOException, DocumentException {
        PdfReader _PdfReader = new PdfReader(sPathPdf);
        int _PDFCountPages = _PdfReader.getNumberOfPages();
        ByteArrayOutputStream _ByteArrayOutputStream = new ByteArrayOutputStream();
        PdfStamper _PdfStamper = new PdfStamper(_PdfReader, _ByteArrayOutputStream);
        PdfGState _PdfGState = new PdfGState();
        _PdfGState.setFillOpacity(1.0f);
        PdfContentByte _PdfContentByte;
        Rectangle _RectangleWM;
        Image _ImageWM = Image.getInstance(sPathImg);
        java.util.Date _Date = new java.util.Date();
        java.text.SimpleDateFormat _SimpleDateformat = new java.text.SimpleDateFormat("dd.MM.yyyy");
        String _StrDate = (_SimpleDateformat.format(_Date));
        Phrase _Phrase = new Phrase(_StrDate, new Font(FontFamily.COURIER, 14f, 0, BaseColor.BLACK));
        for (int n = 1; n <= _PDFCountPages; n++)
        {
            _RectangleWM = _PdfReader.getPageSizeWithRotation(n);
            _PdfContentByte = _PdfStamper.getOverContent(n);
            _PdfContentByte.saveState();
            _PdfContentByte.setGState(_PdfGState);
            _PdfContentByte.addImage(_ImageWM, 180, 0, 0, 30, ((_RectangleWM.getLeft() + _RectangleWM.getRight()) / 2), 5);
            ColumnText.showTextAligned(_PdfContentByte, Element.ALIGN_CENTER, _Phrase, ((_RectangleWM.getLeft() + _RectangleWM.getRight()) / 2) + 118, 12, 0);
            _PdfContentByte.restoreState();
        }
        _PdfStamper.close();
        _PdfReader.close();
        return _ByteArrayOutputStream.toString();
    }
    private static String fAddWMtoPdfm1_outFile(String _PathPDF, String _PathPNG, String _PathOutPDF) throws IOException, DocumentException {
        PdfReader _PdfReader = new PdfReader(_PathPDF);
        int _PDFCountPages = _PdfReader.getNumberOfPages();
        PdfStamper _PdfStamper = new PdfStamper(_PdfReader, new FileOutputStream(_PathOutPDF));
        PdfGState _PdfGState = new PdfGState();
        _PdfGState.setFillOpacity(1.0f);
        PdfContentByte _PdfContentByte;
        Rectangle _RectangleWM;
        Image _ImageWM = Image.getInstance(_PathPNG);
        java.util.Date _Date = new java.util.Date();
        java.text.SimpleDateFormat _SimpleDateformat = new java.text.SimpleDateFormat("dd.MM.yyyy");
        String _StrDate = (_SimpleDateformat.format(_Date));
        Phrase _Phrase = new Phrase(_StrDate, new Font(FontFamily.COURIER, 14f, 0, BaseColor.BLACK));
        for (int n = 1; n <= _PDFCountPages; n++)
        {
            _RectangleWM = _PdfReader.getPageSizeWithRotation(n);
            _PdfContentByte = _PdfStamper.getOverContent(n);
            _PdfContentByte.saveState();
            _PdfContentByte.setGState(_PdfGState);
            _PdfContentByte.addImage(_ImageWM, 180, 0, 0, 30, ((_RectangleWM.getLeft() + _RectangleWM.getRight()) / 2), 5);
            ColumnText.showTextAligned(_PdfContentByte, Element.ALIGN_CENTER, _Phrase, ((_RectangleWM.getLeft() + _RectangleWM.getRight()) / 2) + 118, 12, 0);
            _PdfContentByte.restoreState();
        }
        _PdfStamper.close();
        _PdfReader.close();
        return "success";
    }
    private static String fAddWMtoPdfm2(String sPathPdf, String sPathImg1, String sPathImg2, String sNumberCD) throws IOException, DocumentException {
        PdfReader _PdfReader = new PdfReader(sPathPdf);
        int _PDFCountPages = _PdfReader.getNumberOfPages();
        ByteArrayOutputStream _ByteArrayOutputStream = new ByteArrayOutputStream();
        PdfStamper _PdfStamper = new PdfStamper(_PdfReader, _ByteArrayOutputStream);
        PdfGState _PdfGState = new PdfGState();
        _PdfGState.setFillOpacity(1.0f);
        PdfContentByte _PdfContentByte1, _PdfContentByte2;
        Rectangle _RectangleWM1, _RectangleWM2;
        Image _ImageWM1 = Image.getInstance(sPathImg1);
        Image _ImageWM2 = Image.getInstance(sPathImg2);
        java.util.Date _Date = new java.util.Date();
        java.text.SimpleDateFormat _SimpleDateformat = new java.text.SimpleDateFormat("dd.MM.yyyy");
        String _StrDate = (_SimpleDateformat.format(_Date));
        Phrase _Phrase1 = new Phrase(_StrDate, new Font(FontFamily.COURIER, 14f, 0, BaseColor.BLACK));
        Phrase _Phrase2 = new Phrase(sNumberCD, new Font(FontFamily.COURIER, 14f, 0, BaseColor.BLACK));
        for (int n = 1; n <= _PDFCountPages; n++)
        {
            _RectangleWM1 = _PdfReader.getPageSizeWithRotation(n);
            _PdfContentByte1 = _PdfStamper.getOverContent(n);
            _PdfContentByte1.saveState();
            _PdfContentByte1.setGState(_PdfGState);
            _PdfContentByte1.addImage(_ImageWM1, 180, 0, 0, 30, (_RectangleWM1.getLeft() + 200), 5);
            ColumnText.showTextAligned(_PdfContentByte1, Element.ALIGN_CENTER, _Phrase1, (_RectangleWM1.getLeft() + 200) + 115, 12, 0);
            _PdfContentByte1.restoreState();
            _RectangleWM2 = _PdfReader.getPageSizeWithRotation(n);
            _PdfContentByte2 = _PdfStamper.getOverContent(n);
            _PdfContentByte2.saveState();
            _PdfContentByte2.setGState(_PdfGState);
            _PdfContentByte2.addImage(_ImageWM2, 180, 0, 0, 30, (_RectangleWM2.getLeft() + 10), 5);
            ColumnText.showTextAligned(_PdfContentByte2, Element.ALIGN_CENTER, _Phrase2, (_RectangleWM2.getLeft() + 10) + 115, 12, 0);
            _PdfContentByte1.restoreState();
        }
        _PdfStamper.close();
        _PdfReader.close();
        return _ByteArrayOutputStream.toString();
    }
    private static String fAddWMtoPdfm2_outFile(String sPathPdf, String sPathImg1, String sPathImg2, String sNumberCD, String sPatPdfOUT) throws IOException, DocumentException {
        PdfReader _PdfReader = new PdfReader(sPathPdf);
        int _PDFCountPages = _PdfReader.getNumberOfPages();
        PdfStamper _PdfStamper = new PdfStamper(_PdfReader, new FileOutputStream(sPatPdfOUT));
        PdfGState _PdfGState = new PdfGState();
        _PdfGState.setFillOpacity(1.0f);
        PdfContentByte _PdfContentByte1, _PdfContentByte2;
        Rectangle _RectangleWM1, _RectangleWM2;
        Image _ImageWM1 = Image.getInstance(sPathImg1);
        Image _ImageWM2 = Image.getInstance(sPathImg2);
        java.util.Date _Date = new java.util.Date();
        java.text.SimpleDateFormat _SimpleDateformat = new java.text.SimpleDateFormat("dd.MM.yyyy");
        String _StrDate = (_SimpleDateformat.format(_Date));
        Phrase _Phrase1 = new Phrase(_StrDate, new Font(FontFamily.COURIER, 14f, 0, BaseColor.BLUE));
        Phrase _Phrase2 = new Phrase(sNumberCD, new Font(FontFamily.COURIER, 14f, 0, BaseColor.BLUE));
        for (int n = 1; n <= _PDFCountPages; n++)
        {
            _RectangleWM1 = _PdfReader.getPageSizeWithRotation(n);
            _PdfContentByte1 = _PdfStamper.getOverContent(n);
            _PdfContentByte1.saveState();
            _PdfContentByte1.setGState(_PdfGState);
            _PdfContentByte1.addImage(_ImageWM1, 180, 0, 0, 30, (_RectangleWM1.getLeft() + 200), 5);
            ColumnText.showTextAligned(_PdfContentByte1, Element.ALIGN_CENTER, _Phrase1, (_RectangleWM1.getLeft() + 200) + 115, 12, 0);
            _PdfContentByte1.restoreState();
            _RectangleWM2 = _PdfReader.getPageSizeWithRotation(n);
            _PdfContentByte2 = _PdfStamper.getOverContent(n);
            _PdfContentByte2.saveState();
            _PdfContentByte2.setGState(_PdfGState);
            _PdfContentByte2.addImage(_ImageWM2, 180, 0, 0, 30, (_RectangleWM2.getLeft() + 10), 5);
            ColumnText.showTextAligned(_PdfContentByte2, Element.ALIGN_CENTER, _Phrase2, (_RectangleWM2.getLeft() + 10) + 115, 12, 0);
            _PdfContentByte1.restoreState();
        }
        _PdfStamper.close();
        _PdfReader.close();
        return "fAddWMtoPdfm2_outFile() - success";
    }
}